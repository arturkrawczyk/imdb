from rest_framework import viewsets

from movies import models
from movies import serializers


class MovieViewSet(viewsets.ModelViewSet):
    queryset = models.Movie.objects.all().order_by('primary_title')
    serializer_class = serializers.MovieSerializer

    def get_queryset(self):
        queryset = models.Movie.objects.all().order_by('primary_title')
        start_year = self.request.query_params.get('start_year', None)
        genre = self.request.query_params.get('genre', None)
        person = self.request.query_params.get('person', None)
        if start_year is not None:
            queryset = queryset.filter(start_year=start_year)
        if genre is not None:
            queryset = queryset.filter(genres__contains=[genre])
        if person is not None:
            queryset = queryset.filter(persons__name__icontains=person)
        return queryset


class PersonViewSet(viewsets.ModelViewSet):
    queryset = models.Person.objects.all()
    serializer_class = serializers.PersonSerializer

    def get_queryset(self):
        queryset = models.Person.objects.all()
        person = self.request.query_params.get('person', None)
        if person is not None:
            queryset = queryset.filter(name__icontains=person)
        return queryset
