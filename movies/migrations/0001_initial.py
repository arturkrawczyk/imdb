# Generated by Django 2.2 on 2019-05-01 09:39

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('imdb_id', models.CharField(max_length=20)),
                ('title_type', models.CharField(blank=True, max_length=50, null=True)),
                ('primary_title', models.CharField(max_length=255)),
                ('original_title', models.CharField(max_length=255)),
                ('is_adult', models.BooleanField()),
                ('start_year', models.IntegerField()),
                ('end_year', models.IntegerField(null=True)),
                ('runtime_minutes', models.IntegerField()),
                ('genres', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=20), size=None)),
            ],
        ),
    ]
