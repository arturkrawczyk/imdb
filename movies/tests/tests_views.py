import pytest
from rest_framework import test

from movies import views


@pytest.mark.django_db
class TestMovieListView:

    def setup_method(self):
        self.factory = test.APIRequestFactory()
        self.view = views.MovieViewSet.as_view({'get': 'list'})

    def test_list_view(self):
        request = self.factory.get('/api/movies/')
        response = self.view(request)

        assert response

    def test_list_view_filter_by_start_year(self):
        request = self.factory.get('/api/movies/?start_year=1989')
        response = self.view(request)

        assert response
