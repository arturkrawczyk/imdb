import pytest

from movies import models
from movies import serializers


@pytest.mark.django_db
class TestMovieSerializer:

    def test_serializer(self):
        movie = models.Movie.objects.create(imdb_id='tt1')
        serializer = serializers.MovieSerializer(movie)

        assert serializer.data
