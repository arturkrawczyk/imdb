from movies import utils


def test_get_validated_data_for_invalid_data():
    input_data = '\\N'
    validated_data = utils.get_validated_data(input_data)

    assert validated_data is None


def test_get_validated_data_for_valid_data():
    input_data = 'data'
    validated_data = utils.get_validated_data(input_data)

    assert validated_data == 'data'