from django.conf import settings
import pytest

from movies import importers
from movies import models


def test_get_dataframe_from_gz_file():
    filename = settings.BASE_DIR + '/movies/tests/test_files/test_name.basics.tsv.gz'
    dataframe = importers.get_dataframe_from_gz_file(filename)
    for row in dataframe.itertuples():
        assert row.nconst == 'nm0000001'
        assert row.primaryName == 'Fred Astaire'
        assert row.birthYear == 1899
        assert row.deathYear == 1987
        assert row.primaryProfession == 'soundtrack,actor,miscellaneous'
        assert row.knownForTitles == 'tt0050419,tt0072308,tt0043044,tt0053137'


@pytest.mark.django_db
def test_create_movies_from_title_dataframe():
    filename = settings.BASE_DIR + '/movies/tests/test_files/test_title.basics.tsv.gz'
    title_dataframe = importers.get_dataframe_from_gz_file(filename)
    importers.create_movies_from_title_dataframe(title_dataframe)

    movie = models.Movie.objects.last()

    assert movie.imdb_id == 'tt0000001'
    assert movie.title_type == 'short'
    assert movie.primary_title == 'Carmencita'
    assert movie.original_title == 'Carmencita'
    assert movie.is_adult is False
    assert movie.start_year == 1894
    assert movie.end_year is None
    assert movie.runtime_minutes == 1
    assert movie.genres == ['Documentary', 'Short']


@pytest.mark.django_db
def test_import_titles():
    filename = settings.BASE_DIR + '/movies/tests/test_files/test_title.basics.tsv.gz'
    importers.import_titles_from_file(filename)

    movie = models.Movie.objects.last()

    assert movie.imdb_id == 'tt0000001'
    assert movie.title_type == 'short'
    assert movie.primary_title == 'Carmencita'
    assert movie.original_title == 'Carmencita'
    assert movie.is_adult is False
    assert movie.start_year == 1894
    assert movie.end_year is None
    assert movie.runtime_minutes == 1
    assert movie.genres == ['Documentary', 'Short']


@pytest.mark.django_db
def test_create_persons_from_name_dataframe():
    filename = \
        settings.BASE_DIR + '/movies/tests/test_files/test_name.basics.tsv.gz'
    name_dataframe = importers.get_dataframe_from_gz_file(filename)
    importers.create_persons_from_name_dataframe(name_dataframe)

    person = models.Person.objects.last()

    assert person.imdb_id == 'nm0000001'
    assert person.name == 'Fred Astaire'
    assert person.birth_year == 1899
    assert person.death_year == 1987
    assert person.professions == ['soundtrack', 'actor', 'miscellaneous']


@pytest.mark.django_db
def test_import_names():
    filename = \
        settings.BASE_DIR + '/movies/tests/test_files/test_name.basics.tsv.gz'
    importers.import_names_from_file(filename)

    person = models.Person.objects.last()

    assert person.imdb_id == 'nm0000001'
    assert person.name == 'Fred Astaire'
    assert person.birth_year == 1899
    assert person.death_year == 1987
    assert person.professions == ['soundtrack', 'actor', 'miscellaneous']
