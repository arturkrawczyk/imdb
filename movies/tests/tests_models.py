import pytest

from movies import models


@pytest.mark.django_db
class TestMovie:

    def setup_method(self):
        class TestRow:
            pass
        self.test_row = TestRow()
        self.test_row.tconst = 'tt0000001'
        self.test_row.titleType = 'short'
        self.test_row.primaryTitle = 'Title'
        self.test_row.originalTitle = 'Original title'
        self.test_row.isAdult = 0
        self.test_row.startYear = 1989
        self.test_row.endYear = 1999
        self.test_row.runtimeMinutes = 93
        self.test_row.genres = 'Documentary,Short'

    def test_has_correct_fields(self):
        movie = models.Movie()

        movie.imdb_id = 'tt0000001'
        movie.title_type = 'short'
        movie.primary_title = 'Carmencita'
        movie.original_title = 'Carmencita'
        movie.is_adult = False
        movie.start_year = 1984
        movie.end_year = 1989
        movie.runtime_minutes = 1
        movie.genres = ['Documentary', 'Short']

        movie.save()

        retrieved_movie = models.Movie.objects.last()

        assert retrieved_movie.imdb_id == movie.imdb_id
        assert retrieved_movie.title_type == movie.title_type
        assert retrieved_movie.primary_title == movie.primary_title
        assert retrieved_movie.original_title == movie.original_title
        assert retrieved_movie.is_adult == movie.is_adult
        assert retrieved_movie.start_year == movie.start_year
        assert retrieved_movie.end_year == movie.end_year
        assert retrieved_movie.runtime_minutes == movie.runtime_minutes
        assert retrieved_movie.genres == movie.genres

    def test_create_from_dataframe_row(self):
        models.Movie.create_from_dataframe_row(self.test_row)
        movie = models.Movie.objects.last()

        assert movie.imdb_id == 'tt0000001'
        assert movie.title_type == 'short'
        assert movie.primary_title == 'Title'
        assert movie.original_title == 'Original title'
        assert movie.is_adult is False
        assert movie.start_year == 1989
        assert movie.end_year == 1999
        assert movie.runtime_minutes == 93
        assert movie.genres == ['Documentary', 'Short']

    def test_create_from_dataframe_row_when_no_end_year(self):
        self.test_row.titleType = '\\N'
        self.test_row.primaryTitle = '\\N'
        self.test_row.originalTitle = '\\N'
        self.test_row.isAdult = '\\N'
        self.test_row.startYear = '\\N'
        self.test_row.endYear = '\\N'
        self.test_row.runtimeMinutes = '\\N'
        self.test_row.genres = '\\N'
        models.Movie.create_from_dataframe_row(self.test_row)

        movie = models.Movie.objects.last()

        assert movie.title_type is None
        assert movie.primary_title is None
        assert movie.original_title is None
        assert movie.is_adult is None
        assert movie.start_year is None
        assert movie.end_year is None
        assert movie.runtime_minutes is None
        assert movie.genres is None


@pytest.mark.django_db
class TestPerson:

    def setup_method(self):
        class TestRow:
            pass
        self.test_row = TestRow()
        self.test_row.nconst = 'nm0000001'
        self.test_row.primaryName = 'Fred Astaire'
        self.test_row.birthYear = 1899
        self.test_row.deathYear = 1987
        self.test_row.primaryProfession = 'soundtrack,actor,miscellaneous'
        self.test_row.knownForTitles = 'tt1,tt2,tt3'

    def test_has_correct_fields(self):
        person = models.Person()

        person.imdb_id = 'nm0000001'
        person.name = 'Fred Astaire'
        person.birth_year = 1899
        person.death_year = 1987
        person.professions = ['soundtrack', 'actor', 'miscellaneous']

        person.save()

        retrieved_person = models.Person.objects.last()

        assert retrieved_person.imdb_id == person.imdb_id
        assert retrieved_person.name == person.name
        assert retrieved_person.birth_year == person.birth_year
        assert retrieved_person.death_year == person.death_year
        assert retrieved_person.professions == person.professions

    def test_has_many_to_many_relation_to_movie(self):
        movie_1 = models.Movie.objects.create(
            imdb_id='tt1',
            title_type='type',
            primary_title='primary title 1',
            original_title='original title 1',
            is_adult=False,
            start_year=2000,
            end_year=None,
            runtime_minutes=100,
            genres=[]
        )
        movie_2 = models.Movie.objects.create(
            imdb_id='tt2',
            title_type='type',
            primary_title='primary title 2',
            original_title='original title 2',
            is_adult=False,
            start_year=2000,
            end_year=None,
            runtime_minutes=100,
            genres=[]
        )

        person = models.Person.objects.create(
            imdb_id='nm1',
            name='Name',
            birth_year=1990,
            death_year=None,
            professions=['actor', 'musician']
        )

        person.movies.add(movie_1, movie_2)

        retrieved_person = models.Person.objects.filter(
            movies__in=[movie_1, movie_2]
        ).last()

        assert retrieved_person == person

    def test_create_from_dataframe_row(self):
        movie_1 = models.Movie.objects.create(imdb_id='tt1')
        movie_2 = models.Movie.objects.create(imdb_id='tt2')
        models.Person.create_from_dataframe_row(self.test_row)
        person = models.Person.objects.last()

        assert person.imdb_id == 'nm0000001'
        assert person.name == 'Fred Astaire'
        assert person.birth_year == 1899
        assert person.death_year == 1987
        assert person.professions == ['soundtrack', 'actor', 'miscellaneous']
        assert movie_1 in person.movies.all()
        assert movie_2 in person.movies.all()
