from django import urls
from rest_framework import routers

from movies import views

router = routers.DefaultRouter()
router.register(r'movies', views.MovieViewSet)
router.register(r'persons', views.PersonViewSet)

urlpatterns = [
    urls.path('', urls.include(router.urls)),
]