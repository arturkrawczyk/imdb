from django.contrib.postgres import fields
from django.db import models

from movies import utils


class Movie(models.Model):
    imdb_id = models.CharField(max_length=20, blank=False, null=False)
    title_type = models.CharField(max_length=50, blank=True, null=True)
    primary_title = models.CharField(max_length=255, blank=True, null=True)
    original_title = models.CharField(max_length=255, blank=True, null=True)
    is_adult = models.BooleanField(blank=True, null=True)
    start_year = models.IntegerField(blank=True, null=True)
    end_year = models.IntegerField(blank=True, null=True)
    runtime_minutes = models.IntegerField(blank=True, null=True)
    genres = fields.ArrayField(
        models.CharField(max_length=20), blank=True, null=True
    )

    class Meta:
        indexes = [
            models.Index(fields=['imdb_id'])
        ]

    @classmethod
    def create_from_dataframe_row(cls, row):
        movie = cls()
        movie.imdb_id = row.tconst
        movie.title_type = utils.get_validated_data(row.titleType)
        movie.primary_title = utils.get_validated_data(row.primaryTitle)
        movie.original_title = utils.get_validated_data(row.originalTitle)
        movie.is_adult = utils.get_validated_data(row.isAdult)
        movie.start_year = utils.get_validated_data(row.startYear)
        movie.end_year = utils.get_validated_data(row.endYear)
        movie.runtime_minutes = utils.get_validated_data(row.runtimeMinutes)
        movie.genres = None if row.genres == '\\N' else row.genres.split(',')
        movie.save()
        return movie


class Person(models.Model):
    imdb_id = models.CharField(max_length=20, blank=False, null=False)
    name = models.CharField(max_length=255, blank=True, null=True)
    birth_year = models.IntegerField(blank=True, null=True)
    death_year = models.IntegerField(blank=True, null=True)
    professions = fields.ArrayField(
        models.CharField(max_length=50), blank=True, null=True)
    movies = models.ManyToManyField(Movie, related_name='persons')

    class Meta:
        indexes = [
            models.Index(fields=['imdb_id'])
        ]

    @classmethod
    def create_from_dataframe_row(cls, row):
        person = cls()
        person.imdb_id = row.nconst
        person.name = utils.get_validated_data(row.primaryName)
        person.birth_year = utils.get_validated_data(row.birthYear)
        person.death_year = utils.get_validated_data(row.deathYear)
        person.professions = None if row.primaryProfession == '\\N' \
            else row.primaryProfession.split(',')
        person.save()
        for title in row.knownForTitles.split(','):
            try:
                movie = Movie.objects.get(imdb_id=title)
                person.movies.add(movie)
            except Movie.DoesNotExist:
                print(f'Title with imdb_id {title} does not exist in database')
        return person
