import gzip

import pandas

from movies import models


def get_dataframe_from_gz_file(filename):
    with gzip.open(filename, 'rt') as input_file:
        dataframe = pandas.read_csv(input_file, sep='\t', header=0)
        return dataframe


def create_movies_from_title_dataframe(dataframe):
    for row in dataframe.itertuples():
        try:
            models.Movie.create_from_dataframe_row(row)
        except Exception:
            print(f'Invalid input format for title {row.tconst}')


def create_persons_from_name_dataframe(dataframe):
    for row in dataframe.itertuples():
        try:
            models.Person.create_from_dataframe_row(row)
        except Exception:
            print(f'Invalid input format for name {row.nconst}')


def import_titles_from_file(filename):
    dataframe = get_dataframe_from_gz_file(filename)
    create_movies_from_title_dataframe(dataframe)


def import_names_from_file(filename):
    dataframe = get_dataframe_from_gz_file(filename)
    create_persons_from_name_dataframe(dataframe)
