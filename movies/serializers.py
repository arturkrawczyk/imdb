from rest_framework import serializers

from movies import models


class SimpleMovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Movie
        fields = ('imdb_id', 'primary_title')


class PersonSerializer(serializers.ModelSerializer):
    movies = SimpleMovieSerializer(many=True, read_only=True)

    class Meta:
        model = models.Person
        fields = (
            'imdb_id',
            'name',
            'birth_year',
            'death_year',
            'professions',
            'movies'
        )



class MovieSerializer(serializers.ModelSerializer):
    persons = PersonSerializer(many=True, read_only=True)

    class Meta:
        model = models.Movie
        fields = (
            'imdb_id',
            'title_type',
            'primary_title',
            'original_title',
            'is_adult',
            'start_year',
            'end_year',
            'runtime_minutes',
            'genres',
            'persons',
        )
