"""
This file should not be included in the repo
The data below should be provided in a config during deployment
The reason it's included now is that the deployment itself was not part of
the exercise & it's more convenient for the reader to have this file at hand
rather than for me to keep the purity of keeping it out of the repo
"""
SECRET_KEY = 'b*)f_e0wb&jg-7471yd*x@y!o=wzyt6wrm^@au5paavx0e_h1o'
DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'imdb',
        'USER': 'imdb_user',
        'PASSWORD': '8%3xsdaf_2',
        'HOST': 'localhost',
        'PORT': ''
    }
}
