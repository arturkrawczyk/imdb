from django.conf import urls


urlpatterns = [
    urls.url(r'^api/', urls.include('movies.urls'))
]
