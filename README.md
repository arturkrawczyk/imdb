To set up the project, run following steps:
1) clone the repository into a folder
2) `cd` into the folder & create a `virtualenv` with `python3 -m venv myenv`
3) activate the `virtualenv` by running `source myenv/bin/activate`
4) run `pip install -r requirements.txt`
5) run necessary migrations through `python3 manage.py migrate`

To import files into the database (after setting up the database, 
described below), do the following steps:

1) Open python django shell with `python3 manage.py shell`
2) `from movies import importers`
3) Run `importers.import_titles_from_file(filename)` where `filename` is the full path to `title.basics.tsv.gz` file
3) Run `importers.import_names_from_file(filename)` where `filename` is the full path to `name.basics.tsv.gz` file


To browse api endpoints:

1) Run `python3 manage.py runserver`
2) To see all movies ordered by title, go to `http://127.0.0.1:8000/api/movies/`
3) To see movies filtered by start year, go to `http://127.0.0.1:8000/api/movies/?start_year=1989`
4) To see movies filtered by genre, go to `http://127.0.0.1:8000/api/movies/?genre=Music`
5) To see movies filtered by person, go to `http://127.0.0.1:8000/api/movies/?person=Astaire`
6) To see persons, go to `http://127.0.0.1:8000/api/persons/`
7) To see persons filtered by person, with their related movies, go to `http://127.0.0.1:8000/api/persons/?person=fred`

To run test, go to the root of the project & run `pytest`

Database installation steps for Ubuntu 18.10:

Install postgresql

`sudo apt-get install postgresql postgresql-contrib`

Create database & database user

`sudo su - postgres`

`psql`

In postgresql shell:

`CREATE DATABASE imdb;`

`CREATE USER imdb_user WITH PASSWORD 'password';`

`ALTER ROLE imdb_user SET client_encoding TO 'utf8';`

`ALTER ROLE imdb_user SET default_transaction_isolation TO 'read committed';`

`ALTER ROLE imdb_user SET timezone TO 'UTC';`

`GRANT ALL PRIVILEGES ON DATABASE imdb TO imdb_user;`

`ALTER ROLE imdb_user WITH CREATEDB`
